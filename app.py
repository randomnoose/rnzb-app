import os
import json
import requests

from flask import Flask, render_template
from wtforms import widgets, SelectMultipleField, DecimalField, HiddenField
from flask_wtf import FlaskForm

SECRET_KEY = 'development'

app = Flask(__name__)
app.config.from_object(__name__)
app.config['API_ROOT'] = os.environ.get('API_ROOT', '127.0.0.1:8091')

location_types = [('HOSPITAL', 'Hospital'), ('SUPERMARKET', 'Supermarket'),
        ('GAS_STATION', 'Gas station'), ('HOTEL', 'Hotel'), ('CINEMA', 'Cinema')]
location_types_dict = { 'HOSPITAL': 'Szpital', 'SUPERMARKET': 'Sklep', 'GAS_STATION': 'Stacja paliwowa',
        'HOTEL': 'Hotel/nocleg', 'CINEMA': 'Kino' }

@app.route('/', methods=['GET', 'POST'])
def home_page():
    form = GeoSearchForm()
    if form.validate_on_submit():
        print(form.types_checkboxes.data)
        payload = {
                'lat': form.lat.data,
                'lng': form.lng.data,
                'types': ','.join(str(x) for x in form.types_checkboxes.data)
                }
        r = requests.get('http://{api_root}/api/nearestLocation'.format(api_root=app.config['API_ROOT']), params=payload)
        if r.status_code is 200:
            return render_template('result.html', names_map=location_types_dict, results=r.json())
        else:
            print("Something went wrong: (error)".format(error=r.text))
            return render_template('error.html', error=r.text)
    else:
        pass
    return render_template('index.html', locations=location_types, form=form)

class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

class GeoSearchForm(FlaskForm):
    lat = HiddenField(u'Latitude')
    lng = HiddenField(u'Longitude')
    types_checkboxes = MultiCheckboxField('Label', choices=location_types)
